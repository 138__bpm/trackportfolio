const mongoose = require('mongoose');

let PortFolio = new mongoose.Schema({
    tickers : [{
        name: {type: String},
        quantity: {type: Number},
        averagePrice: {type: Number}
    }]
});

module.exports = mongoose.model('PortFolio', PortFolio);
