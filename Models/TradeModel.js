const mongoose = require('mongoose');

//For simplicity for this assignment we only need a tickerName ,
// no other properties related to ticker
let Trade = new mongoose.Schema({
    tickerName: {type: String, required: true},
    createdAt: {type: Date, default: new Date()},
    type: {type: String, required: true},
    quantity: {type:Number, required:true},
    executionPrice: {type: Number, required: true}
});

module.exports = mongoose.model('Trade', Trade);
