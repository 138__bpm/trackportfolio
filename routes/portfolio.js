const express = require('express');
const Portfolio = require('../Models/PortFolioModel');

const router = express.Router();

router.get('/', async function (req, res, next) {
    let portfolio =  await Portfolio.findOne({}).limit(1);
    res.send(portfolio);
});

router.get('/returns', async function (req, res, next) {
    let portfolio =  await Portfolio.findOne({}).limit(1);
    let returns = 0;
    for(let ticker of portfolio.tickers) {
       returns += (100 - ticker.averagePrice) * ticker.quantity
    }
    res.send({ "returns" : returns})
});

module.exports = router;
