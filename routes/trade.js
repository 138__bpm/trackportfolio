const express = require('express');
const Joi = require('joi');
const httpError = require('http-errors');
const Trade = require('../Models/TradeModel');
const PortFolio = require('../Models/PortFolioModel');
const router = express.Router();

router.post('/', async function (req, res, next) {
    const schema = Joi.object({
        tickerName: Joi.string().required(),
        type: Joi.any().valid('buy', 'sell'),
        quantity: Joi.number().min(1).required(),
        executionPrice: Joi.number().required()
    });
    const input = schema.validate(req.body);
    if (input.error) {
        return next(httpError(400, {message: input.error.details[0].message}))
    }

    let trade = new Trade({
        tickerName: input.value.tickerName,
        type: input.value.type,
        quantity: input.value.quantity,
        executionPrice: input.value.executionPrice
    });
    //Since in this assignment we are only considering 1 user,
    //manually created this users portfolio and then fetching it
    let portfolio = await PortFolio.findOne({}).limit(1);
    let ticker = await PortFolio.find({'tickers.name': input.value.tickerName});
    if (ticker.length === 0) {
      portfolio.tickers.push({
        'name': input.value.tickerName,
        'quantity': input.value.quantity,
        'averagePrice': input.value.executionPrice
      });
      await portfolio.save();
    } else {
        let existingStock = portfolio.tickers.filter(stock => {
           return stock.name === input.value.tickerName
        })[0];
        let newQuantity = existingStock.quantity + (input.value.type === "buy" ? input.value.quantity : -1 * input.value.quantity);
        if(newQuantity < 0) {
            return next(httpError(400, {message: 'Quantity cant be negative or zero'}))
        }
        let newAverage = (existingStock.quantity * existingStock.averagePrice +
            input.value.quantity * input.value.executionPrice) / (existingStock.quantity + input.value.quantity);
        await PortFolio.updateOne({'tickers.name': input.value.tickerName}, {
            '$set': {
                'tickers.$.quantity': newQuantity,
                'tickers.$.averagePrice': newAverage
            }
        })
    }

    let writeResponse = await trade.save();
    res.send(writeResponse);
});

router.get('/getAll', async function (req, res, next) {
    let trades = await Trade.find({});
    res.send(trades);
});
module.exports = router;
