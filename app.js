require('dotenv').config();
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const tradeRouter = require('./routes/trade');
const portfolioRouter = require('./routes/portfolio');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/trade', tradeRouter);
app.use('/portfolio', portfolioRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500).send({"message": err.message});
});

mongoose.connect(process.env.DB_URL)
    .then(() => console.log("Connected to Db"))
    .catch(err => console.log(`Error connecting ${err}`));

module.exports = app;
